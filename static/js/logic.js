// Create the tile layer that will be the background of our map
var lightmap = L.tileLayer("https://api.mapbox.com/styles/v1/mapbox/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "Map data &copy; <a href=\"https://www.openstreetmap.org/\">OpenStreetMap</a> contributors, <a href=\"https://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, Imagery © <a href=\"https://www.mapbox.com/\">Mapbox</a>",
    maxZoom: 18,
    minZoom: 3,
    id: "light-v10",
    accessToken: API_KEY
});


// Initialize all of the LayerGroups we'll be using
var eq_layers = {
    group1: new L.LayerGroup(),
    group2: new L.LayerGroup(),
    group3: new L.LayerGroup(),
    group4: new L.LayerGroup(),
    group5: new L.LayerGroup(),
    group6: new L.LayerGroup()
  };
  
  // Create the map with our layers_ma
  var eq_map = L.map("map", {
    center: [8.21, -0.88],
    zoom: 3,
    'worldCopyJump': true,
    layers: [
      eq_layers.group1,
      eq_layers.group2,
      eq_layers.group3,
      eq_layers.group4,
      eq_layers.group5,
      eq_layers.group6
    ]
    
  });
  
  // Add our 'lightmap' tile layer to the map
  lightmap.addTo(eq_map);
  
  // Create an overlays object to add to the layer control
  var eq_overlays = {
    "4.5": eq_layers.group1,
    "4.6": eq_layers.group2,
    "4.7": eq_layers.group3,
    "4.8": eq_layers.group4,
    "4.9": eq_layers.group5,
    "5.0+": eq_layers.group6
  };

  // Create a control for our layers_ma, add our overlay layers_ma to it
  L.control.layers(null, eq_overlays).addTo(eq_map);
  
  // Create a legend to display information about our map
  var info = L.control({
    position: "bottomright"
  });
  
  // When the layer control is added, insert a div with the class of "legend"
  info.onAdd = function() {
    var div = L.DomUtil.create("div", "legend");
    return div;
  };
  // Add the info legend to the map
  info.addTo(eq_map);
  
  d3.json("https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_week.geojson", function(eqLog) {
  
      // Create an object to keep of the number of markers in each layer
      var magCount = {
        group1: 0,
        group2: 0,
        group3: 0,
        group4: 0,
        group5: 0,
        group6: 0,
      };
  
      var eqMag;
  
      for (var i = 0; i < eqLog.features.length; i++) {
        var eq = eqLog.features[i]

        // If a station is listed but not installed, it's coming soon
        if (eq.properties.mag == 4.5) {
          eqMag = "group1";
        }
        // If a station has no bikes available, it's empty
        else if (eq.properties.mag == 4.6) {
          eqMag = "group2";
        }
        // If a station is installed but isn't renting, it's out of order
        else if (eq.properties.mag == 4.7) {
            eqMag = "group3";
        }
        // If a station has less than 5 bikes, it's status is low
        else if (eq.properties.mag == 4.8) {
            eqMag = "group4";
        }
        // Otherwise the station is normal
        else if (eq.properties.mag == 4.9) {
            eqMag = "group5";
        }
        else {
            eqMag = "group6";
        }
  
        // Update the station count
        magCount[eqMag]++;
        // Create a new marker with the appropriate icon and coordinates
        var newMarker = L.marker([eq.geometry.coordinates[1], eq.geometry.coordinates[0]]);
  
        // Add the new marker to the appropriate layer
        newMarker.addTo(eq_layers[eqMag]);
  
        // Bind a popup to the marker that will  display on click. This will be rendered as HTML
        newMarker.bindPopup("test");
      }
  
      // Call the updateLegend function, which will... update the legend!
      updateLegend(magCount);
    });

  
  // Update the legend's innerHTML with the last updated time and station count
  function updateLegend(magCount) {
    document.querySelector(".legend").innerHTML = [
      "<p class='group1'>Group1: " + magCount.group1 + "</p>",
      "<p class='Campfire'>Campfire: " + magCount.group2 + "</p>",
      "<p class='Children'>Children: " + magCount.group3 + "</p>",
      "<p class='Debris_Burning'>Debris Burning: " + magCount.group4 + "</p>",
      "<p class='Debris_Burning'>Debris Burning: " + magCount.group5 + "</p>",
      "<p class='Other'>Other: " + magCount.group6 + "</p>"
    ].join("");
  }